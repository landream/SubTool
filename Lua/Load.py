#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
import os
from . import LuaCommon
from .. import Common

def run():
	loadUserDefinitionsList()
	
	loadDefinitions(LuaCommon.getDefaultDefinitionsList(), LuaCommon.getDefaultConfigPath())
	
	if LuaCommon.getQuickEnvPath():
		loadDefinitions(LuaCommon.getQuickDefinitionsList(), LuaCommon.getQucikConfigPath())

	sublime.status_message('load definition complete!')

def loadUserDefinitionsList():
	loadDefinitions(LuaCommon.getUserDefinitionsList(), LuaCommon.getUserConfigPath())

def loadDefinitions(lists, jsonpath):
	if not os.path.exists(jsonpath):
		return

	lists.extend(Common.readJson(jsonpath))
