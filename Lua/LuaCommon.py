#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
import os

WORKING_STATE = False
DEFINITIONS_LISTS = [[], [], []]

# work state
def checkWorkState(tip = True):
	if WORKING_STATE and tip:
		sublime.error_message('working !!!')

	return WORKING_STATE

def setWorkState(state):
	global WORKING_STATE
	WORKING_STATE = state


# path config
def getUserPath():
	return os.path.join(sublime.packages_path(), 'User')

def getDefaultPath():
	return ''

def getCachePath():
	return 'lua.cache'

def getConfigPath():
	return os.path.join(getCachePath(), 'user_definition.json')

def getUserCachePath():
	return os.path.join(getUserPath(), getCachePath())

def getUserConfigPath():
	return os.path.join(getUserPath(), getConfigPath())

def getDefaultCachePath():
	return os.path.join(getDefaultPath(), getCachePath())

def getDefaultConfigPath():
	return os.path.join(getDefaultPath(), getConfigPath())

def getQuickEnvPath():
	return os.environ.get('QUICK_V3_ROOT')

def getQucikConfigPath():
	return os.path.join(getDefaultPath(), getCachePath(), 'default_definition.json')

def getDefinitionsLists():
	return DEFINITIONS_LISTS

def getUserDefinitionsList():
	return DEFINITIONS_LISTS[0]

def getDefaultDefinitionsList():
	return DEFINITIONS_LISTS[1]

def getQuickDefinitionsList():
	return DEFINITIONS_LISTS[2]
