#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
from . import LuaCommon
from . import Load
from .. import Common

def run():
	if LuaCommon.checkWorkState():
		return

	Common.cleanPath(LuaCommon.getUserCachePath())
	Common.cleanPath(LuaCommon.getUserConfigPath())
	
	Load.loadUserDefinitionsList()
	
	sublime.message_dialog('clean definition complete!')