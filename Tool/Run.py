#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
import os
import subprocess

def run():
	try:
		project_path = os.path.dirname(sublime.active_window().project_file_name())
		cmd = sublime.active_window().project_data()['build_systems'][0]['cmd']
		cmd = ' '.join(cmd)
		cmd = cmd.replace('${project_path}', project_path)
	except Exception as e:
		return

	info = subprocess.STARTUPINFO()
	info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
	info.wShowWindow = subprocess.SW_HIDE
	subprocess.Popen('cmd.exe /c %s ' % cmd, cwd = project_path, startupinfo = info)
