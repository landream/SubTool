#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
import sublime_plugin
import imp
# imp.reload(Run)

# Tool
from .Tool import Run
from .Tool import Search
from .Tool import Translate

# Lua
from .Lua import Build
from .Lua import Clean
from .Lua import Goto
from .Lua import Load

class SubtoolRunCommand(sublime_plugin.WindowCommand):
	def run(self):
		Run.run()

class SubtoolSearchCommand(sublime_plugin.WindowCommand):
	def run(self):
		Search.run()

class SubtoolTranslateCommand(sublime_plugin.WindowCommand):
	def run(self):
		Translate.run()

class SubtoolBuildLuaDefinitionCommand(sublime_plugin.WindowCommand):
	def run(self, dirs):
		Build.buildDir(dirs)

	def is_visible(self, dirs):
		return len(dirs) > 0

class SubtoolCleanLuaDefinitionCommand(sublime_plugin.WindowCommand):
	def run(self):
		Clean.run()

class SubtoolGotoLuaDefinitionCommand(sublime_plugin.WindowCommand):
	def run(self):
		Goto.run()

class SubtoolEventListener(sublime_plugin.EventListener):
	def on_post_save_async(self, view):
		Build.buildFile(view.file_name())

def SubtoolInit():
	Load.run()

def plugin_loaded():
	sublime.set_timeout(SubtoolInit, 200)
