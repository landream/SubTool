#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime
import os
import shutil
import json

def getText():
	view = sublime.active_window().active_view()
	sel = view.sel()
	if len(sel) == 1:
		pos = sel[0]
		if pos.a == pos.b:
			text = view.substr(view.word(pos))
		else:
			text = view.substr(pos)
	else:
		text = ''

	return text

def cleanPath(path):
	if not os.path.exists(path):
		return
	
	if os.path.isdir(path):
		shutil.rmtree(path, True)
	else:
		os.remove(path)

def makedirs(path):
	if not os.path.exists(path):
		os.makedirs(path)

def readFile(path):
	with open(path, 'r') as f:
		content = f.read()

	return content

def writeFile(path, content):
	with open(path, 'w') as f:
		f.write(content)

def readJson(jsonpath):
	return json.loads(readFile(jsonpath))

def writeJson(filepath, jsonpath):
	writeFile(filepath, json.dumps(jsonpath))

def extname(path):
	return os.path.splitext(path)[1]

def checkExtName(path, ext):
	return extname(path) == ext

def getNoExtName(path):
	return path[:-len(extname(path))]
