##SubTool --v0.0.1
a collection of efficient tool scripts for sublime text 3

##tool
 * **运行** `sublime-project`中的`build_systems`
 * **翻译** 选中单词(有道翻译)
 * **搜索** 选中单词(百度搜索)

##lua
 * **重建** 重建lua自定义自动提示和跳转
 * **清理** 清理lua自定义自动提示和跳转
 * **跳转** 到对应lua文件位置
